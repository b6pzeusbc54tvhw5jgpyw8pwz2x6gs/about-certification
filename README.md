# about-certification

## 1. (for dev) self-signed certification
```
$ ./gen-self-signed-certification.sh my-domain.com
```
`my-domain.com.key` and `my-domain.com.crt` files will be created

## 2. (for production) Certificate Signing Request
```
$ ./gen-csr-for-wildcard-certification.sh my-domain.com
```

`my-domain.com.key` and `my-domain.com.csr` files will be created and
`my-domain.com.csr` file has `*.my-domain.com` and `my-domain.com` as SAN(subjectAltName).
if you need to change other information, please edit script.

## References
- https://www.sslshopper.com/csr-decoder.html
- http://blog.endpoint.com/2014/10/openssl-csr-with-alternative-names-one.html
- http://certificate.fyicenter.com/2098_OpenSSL_req_-distinguished_name_Configuration_Section.html
- https://store.ssl2buy.com/

