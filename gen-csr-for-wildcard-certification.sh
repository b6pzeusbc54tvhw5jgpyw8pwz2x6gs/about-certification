#!/bin/bash

DOMAIN=$1

if [ -z "$DOMAIN" ]
then
  echo Error! read README.md
  exit 1
fi

openssl req -new -sha256 -nodes \
  -out $DOMAIN.csr \
  -newkey rsa:2048 \
  -keyout $DOMAIN.key -config <(
cat <<-EOF

[req]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn
 
[ dn ]
countryName=KR
#stateOrProvinceName=Unknown
#localityName=Unknown
#organizationName=Unknown
#organizationalUnitName=Unknown
#emailAddress=Unkown
commonName=*.$DOMAIN
 
[ req_ext ]
subjectAltName = @alternate_names
 
[ alternate_names ]
DNS.1 = *.$DOMAIN
DNS.2 = $DOMAIN
EOF
)
