#!/bin/bash

DOMAIN=$1

if [ -z "$DOMAIN" ]
then
  echo Error! read README.md
  exit 1
fi

openssl req \
    -x509 \
    -nodes \
    -newkey rsa:2048 \
    -keyout $DOMAIN.key \
    -out $DOMAIN.crt \
    -days 36500 \
    -sha256 \
    -config <(cat <<EOF

[ req ]
prompt = no
distinguished_name = subject
x509_extensions    = x509_ext

[ subject ]
commonName=$DOMAIN
organizationName=for-dev-environment

[ x509_ext ]
subjectAltName = @alternate_names

[ alternate_names ]
DNS.1 = $DOMAIN
EOF
)
